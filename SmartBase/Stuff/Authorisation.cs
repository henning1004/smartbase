﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using SmartBase.ApiModels.Request;
using SmartBase.Entities.Stored;

namespace SmartBase.Stuff
{
    public class Authorisation
    {
        public static Token DoRequest(Grant grant)
        {
            var request = (HttpWebRequest) WebRequest.Create("https://api.amazon.com/auth/o2/token");
            request.Timeout = 7000;

            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
            {
                grant_type = grant.Type,
                code = grant.Code,
                client_id = "amzn1.application-oa2-client.3e87d67306404f1683af406718f0f6aa",
                client_secret = "c96071117b61e4c3b78b96af06f6a2b1ab621b999862c9bdcfeffaeb94aa997f"
            }));
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();


            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            using (var reader = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                string result = reader.ReadToEnd();
                //ToDo Serialisierung vom AcceptGrant anpassen
                return JsonConvert.DeserializeObject<Token>(result);
            }
        }
    }
}