﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace SmartBase.Stuff
{
    public class HttpConnector
    {
        public static string DoRequest(String uri, String user, String pass, object data = null)
        {
            var myContainer = new CookieContainer();
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Credentials = new NetworkCredential(user, pass);
            request.CookieContainer = myContainer;
            request.PreAuthenticate = true;
            request.Timeout = 7000;

            if (data != null)
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                var dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = reader.ReadToEnd();
                    return result;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}