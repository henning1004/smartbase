﻿using SmartBase.Entities;
using System.Collections.Generic;
using SmartBase.Entities.Stored;

namespace SmartBase.Repositories
{
    public class TokenRepo : BaseRepository
    {
        public IEnumerable<Token> FindAll()
        {
            using (var db = GetDatabase())
            {
                var tokens = db.GetCollection<Token>("tokens");
                return tokens.FindAll();
            }
        }

        public Token Find(Token token)
        {
            using (var db = GetDatabase())
            {
                var tokens = db.GetCollection<Token>("tokens");
                return tokens.FindById(token.RefreshToken);
            }
        }

        public Token FindByScope(Scope scope)
        {
            using (var db = GetDatabase())
            {
                var tokens = db.GetCollection<Token>("tokens");
                return tokens.FindOne(token => token.Scope.Equals(scope));
            }
        }

        public string Add(Token token)
        {
            using (var db = GetDatabase())
            {
                var tokens = db.GetCollection<Token>("tokens");
                return tokens.Insert(token);
            }
        }

        public bool Update(Token token)
        {
            using (var db = GetDatabase())
            {
                var tokens = db.GetCollection<Token>("tokens");
                return tokens.Update(token);
            }
        }

        public bool DeleteByRefreshToken(string refreshToken)
        {
            using (var db = GetDatabase())
            {
                var tokens = db.GetCollection<Token>("tokens");
                return tokens.Delete(refreshToken);
            }
        }
    }
}