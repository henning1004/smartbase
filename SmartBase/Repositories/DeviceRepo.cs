﻿using System;
using System.Collections.Generic;
using SmartBase.Entities.Stored;

namespace SmartBase.Repositories
{
    public class DeviceRepo : BaseRepository
    {
        public IEnumerable<Device> FindAll()
        {
            using (var db = GetDatabase())
            {
                var devices = db.GetCollection<Device>("devices");
                return devices.FindAll();
            }
        }

        public Device FindById(Guid id)
        {
            using (var db = GetDatabase())
            {
                var devices = db.GetCollection<Device>("devices");
                return devices.FindById(id);
            }
        }

        public void Add(Device device)
        {
            using (var db = GetDatabase())
            {
                var devices = db.GetCollection<Device>("devices");
                devices.Insert(device);
            }
        }

        public void Update(Device device)
        {
            using (var db = GetDatabase())
            {
                var devices = db.GetCollection<Device>("devices");
                devices.Update(device);
            }
        }

        public void DeleteById(Guid id)
        {
            using (var db = GetDatabase())
            {
                var devices = db.GetCollection<Device>("devices");
                devices.Delete(id);
            }
        }
    }
}