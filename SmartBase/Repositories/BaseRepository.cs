﻿using LiteDB;

namespace SmartBase.Repositories
{
    public abstract class BaseRepository
    {
        protected LiteDatabase GetDatabase()
        {
            return new LiteDatabase(@"MyData.db");
        }
    }
}
