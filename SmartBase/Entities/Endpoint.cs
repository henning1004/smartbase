﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBase.Entities
{
    public class Endpoint
    {
        [JsonProperty("endpointId")]
        public Guid EndpointId { get; set; }

        [JsonProperty("friendlyName")]
        public string FriendlyName { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("manufacturerName")]
        public string ManufacturerName { get; set; }
        
        [JsonProperty("displayCategories", ItemConverterType = typeof(StringEnumConverter))]
        public List<DisplayCategories> DisplayCategories { get; set; }

        [JsonProperty("cookie")]
        public Details Cookie { get; set; }

        [JsonProperty("capabilities")]
        public List<Capability> Capabilities { get; set; }

        [JsonProperty("scope")]
        public Scope Scope { get; set; }

        public class Details
        {
            [JsonProperty("extraDetail1")]
            public string ExtraDetail1 { get; set; }

            [JsonProperty("extraDetail2")]
            public string ExtraDetail2 { get; set; }

            [JsonProperty("extraDetail3")]
            public string ExtraDetail3 { get; set; }

            [JsonProperty("extraDetail4")]
            public string ExtraDetail4 { get; set; }
        }
    }
}