﻿using Newtonsoft.Json;

namespace SmartBase.Entities
{
    public abstract class Capability
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("interface")]
        public string Interface { get; set; }

        [JsonProperty("version")]
        public string Version => "3";

        [JsonProperty("properties")]
        public CapabilityProperty Properties { get; set; }
    }
}
