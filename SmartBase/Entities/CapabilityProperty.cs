﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartBase.Entities
{
    public class CapabilityProperty
    {
        [JsonProperty("supported")]
        public List<Support> Supported { get; set; }

        [JsonProperty("proactivelyReported")]
        public bool ProactivelyReported { get; set; }

        [JsonProperty("retrievable")]
        public bool Retrievable { get; set; }

        public class Support
        {
            [JsonProperty("name")]
            public string Name { get; set; }
        }
    }
}
