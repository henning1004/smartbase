﻿using System;
using System.Collections.Generic;
using SmartBase.ApiModels.Response;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities.Stored;
using SmartBase.Stuff;

namespace SmartBase.Entities.Capabilities
{
    public class PercentageController : Capability
    {
        // muss dynamisch werden
        public PercentageController()
        {
            Type = "AlexaInterface";
            Interface = "Alexa.PercentageController  ";
            Properties = new CapabilityProperty
            {
                Supported = new List<CapabilityProperty.Support>
                {
                    new CapabilityProperty.Support {Name = "percentage"}
                },
                Retrievable = true
            };
        }

        public static List<StateReportProperty> HandleSetPercentage(Device device, int percentage)
        {
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/setPowerLevel", "admin", "admin", percentage);
            
           return new List<StateReportProperty>
            {
                new Percentage(Convert.ToInt32(value), "Alexa.PercentageController ")
            };
        }

        public static List<StateReportProperty> HandleAdjustPercentage(Device device, int percentageDelta)
        {
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/adjustPowerLevel", "admin", "admin", percentageDelta);
            
            return new List<StateReportProperty>
            {
                new Percentage(Convert.ToInt32(value), "Alexa.PercentageController ")
            };
        }
    }
}