﻿using System;
using System.Collections.Generic;
using SmartBase.ApiModels.Response;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities.Stored;
using SmartBase.Stuff;

namespace SmartBase.Entities.Capabilities
{
    public class PowerController : Capability
    {
        // muss dynamisch werden
        public PowerController()
        {
            Type = "AlexaInterface";
            Interface = "Alexa.PowerController";
            Properties = new CapabilityProperty
            {
                Supported = new List<CapabilityProperty.Support>
                {
                    new CapabilityProperty.Support {Name = "powerState"}
                },
                Retrievable = true
            };
        }

        public static List<StateReportProperty> HandlePowerControl(Device device, string requestMethod)
        {
            string value = null;
            if (requestMethod == "TurnOn")
            {
                value = HttpConnector.DoRequest("http://" + device.Ip + "/turnOn", "admin", "admin");
            }
            else if (requestMethod == "TurnOff")
            {
                value = HttpConnector.DoRequest("http://" + device.Ip + "/turnOff", "admin", "admin");
            }

            return new List<StateReportProperty>
            {
                new PowerState(Enum.Parse<PowerState.Values>(value), "Alexa.PowerController")
            };
        }
    }
}