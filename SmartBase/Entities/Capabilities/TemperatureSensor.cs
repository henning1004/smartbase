﻿using System.Collections.Generic;

namespace SmartBase.Entities.Capabilities
{
    public class TemperatureSensor : Capability
    {
        // muss dynamisch werden
        public TemperatureSensor()
        {
            Type = "AlexaInterface";
            Interface = "Alexa.TemperatureSensor";
            Properties = new CapabilityProperty
            {
                Supported = new List<CapabilityProperty.Support>
                {
                    new CapabilityProperty.Support {Name = "temperature"}
                },
                Retrievable = true
            };
        }
    }
}
