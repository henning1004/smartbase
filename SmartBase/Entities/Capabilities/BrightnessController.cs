﻿using System;
using System.Collections.Generic;
using SmartBase.ApiModels.Response;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities.Stored;
using SmartBase.Stuff;

namespace SmartBase.Entities.Capabilities
{
    public class BrightnessController : Capability
    {
        // muss dynamisch werden
        public BrightnessController()
        {
            Type = "AlexaInterface";
            Interface = "Alexa.BrightnessController";
            Properties = new CapabilityProperty
            {
                Supported = new List<CapabilityProperty.Support>
                {
                    new CapabilityProperty.Support {Name = "brightness"}
                },
                Retrievable = true
            };
        }

        public static List<StateReportProperty> HandleSetBrightness(Device device, int brightness)
        {
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/setPowerLevel", "admin", "admin", brightness);

            return new List<StateReportProperty>
            {
                new Brightness(Convert.ToInt32(value), "Alexa.BrightnessController")
            };
        }

        public static List<StateReportProperty> HandleAdjustBrightness(Device device, int brightnessDelta)
        {
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/adjustPowerLevel", "admin", "admin", brightnessDelta);

            return new List<StateReportProperty>
            {
                new Brightness(Convert.ToInt32(value), "Alexa.BrightnessController")
            };
        }
    }
}