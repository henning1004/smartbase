﻿using System;
using System.Collections.Generic;
using SmartBase.ApiModels.Response;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities.Stored;
using SmartBase.Stuff;

namespace SmartBase.Entities.Capabilities
{
    public class PowerLevelController : Capability
    {
        // muss dynamisch werden
        public PowerLevelController()
        {
            Type = "AlexaInterface";
            Interface = "Alexa.PowerLevelController ";
            Properties = new CapabilityProperty
            {
                Supported = new List<CapabilityProperty.Support>
                {
                    new CapabilityProperty.Support {Name = "powerLevel"}
                },
                Retrievable = true
            };
        }

        public static List<StateReportProperty> HandleSetPowerLevel(Device device, int powerLevel)
        {
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/setPowerLevel", "admin", "admin", powerLevel);

            return new List<StateReportProperty>
            {
                new PowerLevel(Convert.ToInt32(value), "Alexa.PowerLevelController")
            };
        }

        public static List<StateReportProperty> HandleAdjustPowerLevel(Device device, int powerLevelDelta)
        {
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/adjustPowerLevel", "admin", "admin", powerLevelDelta);

            return new List<StateReportProperty>
            {
                new PowerLevel(Convert.ToInt32(value), "Alexa.PowerLevelController")
            };
        }
    }
}