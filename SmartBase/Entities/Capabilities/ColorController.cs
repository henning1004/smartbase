﻿using System;
using System.Collections.Generic;
using SmartBase.ApiModels.Response;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities.Stored;
using SmartBase.Stuff;

namespace SmartBase.Entities.Capabilities
{
    public class ColorController : Capability
    {
        // muss dynamisch werden
        public ColorController()
        {
            Type = "AlexaInterface";
            Interface = "Alexa.ColorController";
            Properties = new CapabilityProperty
            {
                Supported = new List<CapabilityProperty.Support>
                {
                    new CapabilityProperty.Support {Name = "color"}
                },
                Retrievable = true
            };
        }

        public static List<StateReportProperty> HandleColorControl(Device device, Color.ValueClass color)
        {
            var rgb = HsvToRgb(new Hsv(color.Hue, color.Saturation, color.Brightness));
            //ToDo RGB Value konvertieren
            var value = HttpConnector.DoRequest("http://" + device.Ip + "/setColor", "admin", "admin", rgb);
            
            return new List<StateReportProperty>
            {
                new Color(0,0,0, "Alexa.ColorController")
            };
        }

        public struct Rgb
        {
            public Rgb(byte r, byte g, byte b)
            {
                R = r;
                G = g;
                B = b;
            }

            public byte R { get; set; }

            public byte G { get; set; }

            public byte B { get; set; }

            public bool Equals(Rgb rgb)
            {
                return R == rgb.R && G == rgb.G && B == rgb.B;
            }
        }

        public struct Hsv
        {
            public Hsv(double h, double s, double v)
            {
                H = h;
                S = s;
                V = v;
            }

            public double H { get; set; }

            public double S { get; set; }

            public double V { get; set; }

            public bool Equals(Hsv hsv)
            {
                return H.Equals(hsv.H) && S.Equals(hsv.S) && V.Equals(hsv.V);
            }
        }

        public static Rgb HsvToRgb(Hsv hsv)
        {
            double r, g, b;

            if (hsv.S.Equals(0))
            {
                r = hsv.V;
                g = hsv.V;
                b = hsv.V;
            }
            else
            {
                int i;
                double f, p, q, t;

                if (hsv.H.Equals(360))
                    hsv.H = 0;
                else
                    hsv.H = hsv.H / 60;

                i = (int)Math.Truncate(hsv.H);
                f = hsv.H - i;

                p = hsv.V * (1.0 - hsv.S);
                q = hsv.V * (1.0 - hsv.S * f);
                t = hsv.V * (1.0 - hsv.S * (1.0 - f));

                switch (i)
                {
                    case 0:
                        r = hsv.V;
                        g = t;
                        b = p;
                        break;

                    case 1:
                        r = q;
                        g = hsv.V;
                        b = p;
                        break;

                    case 2:
                        r = p;
                        g = hsv.V;
                        b = t;
                        break;

                    case 3:
                        r = p;
                        g = q;
                        b = hsv.V;
                        break;

                    case 4:
                        r = t;
                        g = p;
                        b = hsv.V;
                        break;

                    default:
                        r = hsv.V;
                        g = p;
                        b = q;
                        break;
                }
            }

            return new Rgb((byte)(r * 255), (byte)(g * 255), (byte)(b * 255));
        }
    }
}
