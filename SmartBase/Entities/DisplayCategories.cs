﻿
// ReSharper disable InconsistentNaming
namespace SmartBase.Entities
{
    public enum DisplayCategories
    {
        LIGHT,
        SMARTPLUG,
        SWITCH,
        ACTIVITY_TRIGGER,
        CAMERA,
        DOOR,
        OTHER,
        SCENE_TRIGGER,
        SMARTLOCK,
        SPEAKER,
        TEMPERATURE_SENSOR,
        THERMOSTAT,
        TV
    }
}