﻿using LiteDB;

namespace SmartBase.Entities.Stored
{
    public class Token 
    {
        public Scope Scope { get; set; }

        public long ExpiresIn { get; set; }

        [BsonId]
        public string RefreshToken { get; set; }
    }
}
