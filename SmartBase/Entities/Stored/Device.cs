﻿using System;

namespace SmartBase.Entities.Stored
{
    public class Device
    {
        public Guid Id { get; set; }

        public Endpoint Endpoint { get; set; }
        
        public string Ip { get; set; }
    }
}