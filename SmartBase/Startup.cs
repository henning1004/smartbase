﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SmartBase.Repositories;

namespace SmartBase
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddMvcCore().AddJsonFormatters();
            //services.AddKendo(); für HTML-Helper
            services.AddSingleton<DeviceRepo>();
            services.AddSingleton<TokenRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            //app.UseMiddleware<AuthenticationMiddleware>(); replaced by BasicAuthenticationAttribute

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Device}/{action=List}/{id?}");
            });

            //app.UseKendo(env); für HTML-Helper
        }
    }
}
