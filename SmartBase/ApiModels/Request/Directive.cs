﻿using Newtonsoft.Json;
using SmartBase.Entities;

namespace SmartBase.ApiModels.Request
{
    
    public class Directive
    {
        [JsonProperty("header")]
        public Header Header { get; set; }

        [JsonProperty("endpoint")]
        public Endpoint Endpoint { get; set; }

        [JsonProperty("payload")]
        public Payload Payload { get; set; }
    }
}
