﻿using System.Collections.Generic;
using Newtonsoft.Json;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities;

namespace SmartBase.ApiModels.Request
{
    public class Payload
    {
        [JsonProperty("endpoints")]
        public List<Endpoint> Endpoints = new List<Endpoint>();

        [JsonProperty("scope")]
        public Scope Scope { get; set; }

        [JsonProperty("color")]
        public Color.ValueClass Color { get; set; }

        [JsonProperty("brightness")]
        public int Brightness { get; set; }

        [JsonProperty("brightnessDelta")]
        public int BrightnessDelta { get; set; }

        [JsonProperty("powerLevel")]
        public int PowerLevel { get; set; }

        [JsonProperty("powerLevelDelta")]
        public int PowerLevelDelta { get; set; }

        [JsonProperty("percentage")]
        public int Percentage { get; set; }

        [JsonProperty("percentageDelta")]
        public int PercentageDelta { get; set; }

        [JsonProperty("grant")]
        public Grant Grant { get; set; }

        [JsonProperty("grantee")]
        public Grantee Grantee { get; set; }
    }
}