﻿using LiteDB;
using Newtonsoft.Json;

namespace SmartBase.ApiModels.Request
{
    public class Grantee
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [BsonId]
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
