﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Request
{
    public class Context
    {
        [JsonProperty("callbackWaitsForEmptyEventLoop")]
        public bool CallbackWaitsForEmptyEventLoop { get; set; }

        [JsonProperty("logGroupName")]
        public string LogGroupName { get; set; }

        [JsonProperty("logStreamName")]
        public string LogStreamName { get; set; }

        [JsonProperty("functionName")]
        public string FunctionName { get; set; }

        [JsonProperty("memoryLimitInMB")]
        public string MemoryLimitInMb { get; set; }

        [JsonProperty("functionVersion")]
        public string FunctionVersion { get; set; }

        [JsonProperty("invokeid")]
        public string Invokeid { get; set; }

        [JsonProperty("awsRequestId")]
        public string AwsRequestId { get; set; }

        [JsonProperty("invokedFunctionArn")]
        public string InvokedFunctionArn { get; set; }
    }
}