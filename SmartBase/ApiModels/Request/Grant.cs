﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Request
{
    public class Grant
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
    }
}
