﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Request
{
    public class AlexaEvent
    {
        [JsonProperty("request")]
        public Request Request { get; set; }

        [JsonProperty("context")]
        public Context Context { get; set; }
    }
}