﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Request
{
    public class Request
    {
        [JsonProperty("directive")]
        public Directive Directive { get; set; }
    }
}
