﻿using System;
using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response
{
    public abstract class StateReportProperty
    {
        protected StateReportProperty()
        {
            UncertaintyInMilliseconds = 5000;
        }

        [JsonProperty("namespace")]
        public string Namespace { get; set; }

        [JsonProperty("name")]
        public abstract string Name { get; }

        [JsonProperty("timeOfSample")]
        public string TimeOfSample => DateTime.UtcNow.ToString("o"); 

        [JsonProperty("uncertaintyInMilliseconds")]
        public int UncertaintyInMilliseconds { get; set; }
    }
}
