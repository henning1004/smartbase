﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class PowerState : StateReportProperty
    {
        public PowerState(Values value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "powerState";

        [JsonProperty("value" )]
        [JsonConverter(typeof(StringEnumConverter))]
        public Values Value { get; set; }

        public enum Values
        {
            // ReSharper disable InconsistentNaming
            ON,
            OFF
        }
    }
}
