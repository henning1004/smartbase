﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class ThermostatMode : StateReportProperty
    {
        public ThermostatMode(Values value, string nameSpace)
        {
            if (value.Equals(Values.CUSTOM))
            {
                throw new ArgumentException("customName is required when value is set to CUSTOM");
            }
            Value = value;
            Namespace = nameSpace;
        }

        public ThermostatMode(Values value, string customName, string nameSpace)
        {
            Value = new ValueClass
            {
                Value = value,
                CustomName = customName
            };
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "lockState";

        [JsonProperty("value")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Object Value { get; set; }

        public enum Values
        {
            // ReSharper disable InconsistentNaming
            AUTO,
            COOL,
            HEAT,
            ECO,
            OFF,
            CUSTOM
        }

        public class ValueClass
        {
            [JsonProperty("value")]
            public Values Value { get; set; }

            [JsonProperty("scale")]
            public string CustomName { get; set; }
        }
    }
}
