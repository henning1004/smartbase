﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Percentage : StateReportProperty
    {
        public Percentage(int value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "percentage";

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
