﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Connectivity : StateReportProperty
    {
        public Connectivity(Values value, string nameSpace)
        {
            Value = new ValueClass
            {
                Value = value
            };
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "connectivity";

        [JsonProperty("value")]
        public ValueClass Value { get; set; }

        public class ValueClass
        {
            [JsonProperty("value")]
            [JsonConverter(typeof(StringEnumConverter))]
            public Values Value { get; set; }
        }

        public enum Values
        {
            // ReSharper disable InconsistentNaming
            OK,
            UNREACHABLE
        }
    }
}
