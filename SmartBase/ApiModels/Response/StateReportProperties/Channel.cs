﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Channel : StateReportProperty
    {
        public Channel(string number, string callSign, string affiliateCallSign, string nameSpace)
        {
            Value = new ValueClass
            {
                AffiliateCallSign = affiliateCallSign,
                CallSign = callSign,
                Number = number
            };
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "channel";

        [JsonProperty("value")]
        public ValueClass Value { get; set; }

        public class ValueClass
        {
            [JsonProperty("number")]
            public string Number { get; set; }

            [JsonProperty("callSign")]
            public string CallSign { get; set; }

            [JsonProperty("affiliateCallSign")]
            public string AffiliateCallSign { get; set; }
        }
    }
}
