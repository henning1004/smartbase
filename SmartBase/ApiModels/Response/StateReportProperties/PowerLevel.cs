﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class PowerLevel : StateReportProperty
    {
        public PowerLevel(int value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "powerLevel";

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
