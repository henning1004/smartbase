﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Input : StateReportProperty
    {
        public Input(string value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "input";

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
