﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Brightness : StateReportProperty
    {
        public Brightness(int value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "brightness";

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
