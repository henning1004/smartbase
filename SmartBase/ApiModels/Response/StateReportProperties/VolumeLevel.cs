﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class VolumeLevel : StateReportProperty
    {
        public VolumeLevel(string value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "volume";

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
