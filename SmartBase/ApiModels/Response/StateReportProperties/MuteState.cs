﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class MuteState : StateReportProperty
    {
        public MuteState(bool value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "muted";

        [JsonProperty("value")]
        public bool Value { get; set; }
    }
}
