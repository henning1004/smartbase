﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class ColorTemperatureInKelvin : StateReportProperty
    {
        public ColorTemperatureInKelvin(int value, string nameSpace)
        {
            Value = value;
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "colorTemperatureInKelvin";

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
