﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Color : StateReportProperty
    {
        public Color(double hue, double saturation, double brightness, string nameSpace)
        {
            Value = new ValueClass
            {
                Brightness = brightness,
                Hue = hue,
                Saturation = saturation
            };
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "color";

        [JsonProperty("value")]
        public ValueClass Value { get; set; }

        public class ValueClass
        {
            [JsonProperty("hue")]
            public double Hue { get; set; }

            [JsonProperty("saturation")]
            public double Saturation { get; set; }

            [JsonProperty("brightness")]
            public double Brightness { get; set; }
        }
    }
}
