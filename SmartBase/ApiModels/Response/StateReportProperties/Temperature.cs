﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBase.ApiModels.Response.StateReportProperties
{
    public class Temperature : StateReportProperty
    {
        public Temperature(double value, Values scale, string nameSpace)
        {
            Value = new ValueClass
            {
                Value = value,
                Scale = scale
            };
            Namespace = nameSpace;
        }

        [JsonProperty("name")]
        public override string Name => "lowerSetpoint";

        [JsonProperty("value")]
        public ValueClass Value { get; set; }

        public class ValueClass
        {
            [JsonProperty("value")]
            public double Value { get; set; }

            [JsonProperty("scale")]
            [JsonConverter(typeof(StringEnumConverter))]
            public Values Scale { get; set; }
        }

        public enum Values
        {
            // ReSharper disable InconsistentNaming
            CELSIUS,
            FAHRENHEIT,
            KELVIN
        }
    }
}
