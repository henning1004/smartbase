﻿using Newtonsoft.Json;

namespace SmartBase.ApiModels
{
    public class Header
    {
        [JsonProperty("messageId")]
        public string MessageId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("namespace")]
        public string Namespace { get; set; }

        [JsonProperty("payloadVersion")]
        public string PayloadVersion { get; set; }

        [JsonProperty("correlationToken")]
        public string CorrelationToken { get; set; }
    }
}
