﻿using System.Collections.Generic;
using SmartBase.Entities.Stored;

namespace SmartBase.Models
{
    public class DevicesModel
    {
        public IEnumerable<Device> Devices { get; set; }
    }
}
