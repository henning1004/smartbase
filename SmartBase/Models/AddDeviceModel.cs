﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartBase.Entities;

namespace SmartBase.Models
{
    public class AddDeviceModel
    {
        public Guid? Id { get; set; }

        public Endpoint Endpoint { get; set; }

        public string Ip { get; set; }

        public List<string> Capabilities { get; set; }

        public SelectList CapabilityData { get; set; }

        public SelectList DisplayCategoryData
        {
            get
            {
                return new SelectList(Enum.GetValues(typeof(DisplayCategories))
                    .OfType<Enum>()
                    .Select(x => new SelectListItem
                    {
                        Text = Enum.GetName(typeof(DisplayCategories), x),
                        Value = (Convert.ToInt32(x))
                            .ToString()
                    }), "Value", "Text");
            }
        }
    }
}