﻿using System;
using System.Collections.Generic;
using SmartBase.ApiModels.Response;
using SmartBase.ApiModels.Response.StateReportProperties;
using SmartBase.Entities.Capabilities;
using SmartBase.Entities.Stored;
using SmartBase.Stuff;

namespace SmartBase.Controllers.Functions
{
    public static class ReportState
    {
        public static List<StateReportProperty> HandleReportState(Device device)
        {
            var properties = new List<StateReportProperty>();
            string powerState = null;
            string temperature = null;
            if (device.Endpoint.Capabilities.Exists(capability => capability.Interface.Equals("Alexa." + nameof(PowerController))))
            {
                powerState = HttpConnector.DoRequest("http://" + device.Ip + "/getPowerState", "admin", "admin");
            }
            if (device.Endpoint.Capabilities.Exists(capability => capability.Interface.Equals("Alexa." + nameof(TemperatureSensor))))
            {
                temperature = HttpConnector.DoRequest("http://" + device.Ip + "/getTemperature", "admin", "admin");
            }

            if (powerState != null)
            {
                properties.Add(new PowerState(Enum.Parse<PowerState.Values>(powerState), "Alexa.PowerController"));
            }
            if (temperature != null)
            {
                properties.Add(new Temperature(Convert.ToDouble(temperature), Temperature.Values.CELSIUS,
                    "Alexa.TemperatureSensor"));
            }

            return properties;
        }
    }
}