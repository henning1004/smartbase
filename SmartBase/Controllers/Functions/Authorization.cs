﻿using System;
using Newtonsoft.Json;
using SmartBase.ApiModels;
using SmartBase.ApiModels.Request;
using SmartBase.Repositories;
using SmartBase.Stuff;

namespace SmartBase.Controllers.Functions
{
    public static class Authorization
    {
        public static string HandleAuthorization(Request request, TokenRepo tokenRepo)
        {
            var grant = request.Directive.Payload.Grant;
            //ToDo Prüfen was es mit Gtrantee auf sich hat
            var grantee = request.Directive.Payload.Grantee;

            var error = false;

            var token = Authorisation.DoRequest(grant);

            // save data
            if (tokenRepo.Find(token) == null)
            {
                tokenRepo.Add(token);
            }
            else
            {
                error = true;
            }

            // build data for response
            Header header;
            object payload;
            if (!error)
            {
                header = new Header
                {
                    Namespace = "Alexa.Authorization",
                    Name = "AcceptGrant.Response",
                    MessageId = Guid.NewGuid().ToString(),
                    PayloadVersion = "3"
                };

                payload = null;
            }
            else
            {
                header = new Header
                {
                    Namespace = "Alexa.Authorization",
                    Name = "ErrorResponse",
                    MessageId = Guid.NewGuid().ToString(),
                    PayloadVersion = "3"
                };

                payload = new
                {
                    type = "ACCEPT_GRANT_FAILED",
                    message = "Failed to handle the AcceptGrant directive because ..."
                };
            }

            return JsonConvert.SerializeObject(new { @event = new { header, payload } },
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }
    }
}
