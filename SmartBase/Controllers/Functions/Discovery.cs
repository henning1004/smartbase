﻿using System;
using System.Linq;
using Newtonsoft.Json;
using SmartBase.ApiModels;
using SmartBase.ApiModels.Request;
using SmartBase.Repositories;

namespace SmartBase.Controllers.Functions
{
    public static class Discovery
    {
        public static string HandleDiscovery(Request request, DeviceRepo deviceRepo)
        {
            // ToDo Authenifizierung beim Discovery
            var token = request.Directive.Payload.Scope.Token;
            if (token == "")
                return "not authenticated";

            // load data
            var devices = deviceRepo.FindAll();

            // build data for response
            var header = new Header
            {
                Namespace = "Alexa.Discovery",
                Name = "Discover.Response",
                MessageId = Guid.NewGuid().ToString(),
                PayloadVersion = "3",
                CorrelationToken = request.Directive.Header.CorrelationToken
            };

            var endpoints = devices.Select(d => d.Endpoint);

            return JsonConvert.SerializeObject(new { @event = new { header, payload = new { endpoints } } },
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }
    }
}
