﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SmartBase.ApiModels;
using SmartBase.ApiModels.Request;
using SmartBase.ApiModels.Response;
using SmartBase.Controllers.Functions;
using SmartBase.Entities.Capabilities;
using SmartBase.Entities.Stored;
using SmartBase.Repositories;
using SmartBase.Stuff;

namespace SmartBase.Controllers
{
    [BasicAuthentication("test", "test")]
    public class AlexaController : Controller
    {
        readonly TokenRepo _tokenRepo;
        readonly DeviceRepo _deviceRepo;

        public AlexaController(DeviceRepo deviceRepo, TokenRepo tokenRepo)
        {
            _tokenRepo = tokenRepo;
            _deviceRepo = deviceRepo;
        }

        public string Alexa([FromBody] AlexaEvent alexaEvent)
        {
            if (alexaEvent?.Context == null || alexaEvent.Context.InvokedFunctionArn !=
                "arn:aws:lambda:eu-west-1:855711500677:function:mySmartHomeFunction")
            {
                // ToDo Error Response
                return "request error";
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa.Authorization" &&
                alexaEvent.Request.Directive.Header.Name == "AcceptGrant")
            {
                return Authorization.HandleAuthorization(alexaEvent.Request, _tokenRepo);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa.Discovery")
            {
                return Discovery.HandleDiscovery(alexaEvent.Request, _deviceRepo);
            }

            // ToDo Authentifizierung beim Geräte schalten
            /*
            var token = request.Directive.Endpoint.Scope.Token;
            if (token == "")
            return "not authenticated";                 
             */


            if ((alexaEvent.Request.Directive.Header.Namespace == "Alexa" &&
                alexaEvent.Request.Directive.Header.Name == "ReportState") ||
                alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(TemperatureSensor))
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties =
                    ReportState.HandleReportState(device);
                return BuildReportState(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(PowerController) &&
                (alexaEvent.Request.Directive.Header.Name == "TurnOn" ||
                 alexaEvent.Request.Directive.Header.Name == "TurnOff"))
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = PowerController.HandlePowerControl(device, alexaEvent.Request.Directive.Header.Name);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(ColorController) &&
                alexaEvent.Request.Directive.Header.Name == "SetColor")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = ColorController.HandleColorControl(device, alexaEvent.Request.Directive.Payload.Color);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(BrightnessController) &&
                alexaEvent.Request.Directive.Header.Name == "SetBrightness")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = BrightnessController.HandleSetBrightness(device, alexaEvent.Request.Directive.Payload.Brightness);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(BrightnessController) &&
                alexaEvent.Request.Directive.Header.Name == "AdjustBrightness")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = BrightnessController.HandleAdjustBrightness(device, alexaEvent.Request.Directive.Payload.BrightnessDelta);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(PercentageController) &&
                alexaEvent.Request.Directive.Header.Name == "SetPercentage")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = PercentageController.HandleSetPercentage(device, alexaEvent.Request.Directive.Payload.Percentage);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(PercentageController) &&
                alexaEvent.Request.Directive.Header.Name == "AdjustPercentage")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = PercentageController.HandleAdjustPercentage(device, alexaEvent.Request.Directive.Payload.PercentageDelta);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(PowerLevelController) &&
                alexaEvent.Request.Directive.Header.Name == "SetPowerLevel")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = PowerLevelController.HandleSetPowerLevel(device, alexaEvent.Request.Directive.Payload.PowerLevel);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            if (alexaEvent.Request.Directive.Header.Namespace == "Alexa." + nameof(PowerLevelController) &&
                alexaEvent.Request.Directive.Header.Name == "AdjustPowerLevel")
            {
                var device = LoadDevice(alexaEvent.Request.Directive.Endpoint.EndpointId);
                var properties = PowerLevelController.HandleAdjustPowerLevel(device, alexaEvent.Request.Directive.Payload.PowerLevelDelta);
                return BuildResponse(device, properties, alexaEvent.Request.Directive.Header.CorrelationToken);
            }

            return "not implemented";
        }

        private Device LoadDevice(Guid endpointId)
        {
            var device = _deviceRepo.FindById(endpointId);
            if (device == null)
            {
                TextWriter errorWriter = Console.Error;
                errorWriter.WriteLine("Device " + endpointId + " not found!");
            }
            return device;
        }

        private string BuildResponse(Device device, List<StateReportProperty> properties, string correlationToken)
        {
            var header = new Header
            {
                Name = "Response",
                Namespace = "Alexa",
                MessageId = Guid.NewGuid().ToString(),
                CorrelationToken = correlationToken,
                PayloadVersion = "3"
            };

            var endpoint = device.Endpoint;

            return JsonConvert.SerializeObject(new {context = new {properties}, @event = new {header, endpoint}},
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
        }

        private string BuildReportState(Device device, List<StateReportProperty> properties, string correlationToken)
        {
            var header = new Header
            {
                Name = "StateReport",
                Namespace = "Alexa",
                MessageId = Guid.NewGuid().ToString(),
                CorrelationToken = correlationToken,
                PayloadVersion = "3"
            };

            var endpoint = device.Endpoint;

            return JsonConvert.SerializeObject(new {context = new {properties}, @event = new {header, endpoint}},
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
        }
    }
}