﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartBase.Entities;
using SmartBase.Entities.Capabilities;
using SmartBase.Entities.Stored;
using SmartBase.Models;
using SmartBase.Repositories;
namespace SmartBase.Controllers
{
    public class DeviceController : Controller
    {
        readonly DeviceRepo _deviceRepo;

        public DeviceController(DeviceRepo deviceRepo)
        {
            _deviceRepo = deviceRepo;
        }

        public IActionResult List()
        {
            return View(new DevicesModel
            {
                Devices = _deviceRepo.FindAll()
            });
        }

        public IActionResult Add()
        {
            var data = new AddDeviceModel
            {
                CapabilityData = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "PowerController", Value = "PowerController"},
                    new SelectListItem {Text = "ColorController", Value = "ColorController"},
                    new SelectListItem {Text = "BrightnessController", Value = "BrightnessController"},
                    new SelectListItem {Text = "PowerLevelController ", Value = "PowerLevelController "},
                    new SelectListItem {Text = "PercentageController ", Value = "PercentageController "},
                    new SelectListItem {Text = "TemperatureSensor", Value = "TemperatureSensor"}
                }, "Value", "Text")
            };

            return View(data);
        }

        [HttpPost]
        public IActionResult Save(AddDeviceModel data)
        {
            var id = Guid.NewGuid();
            if (data.Id.HasValue)
            {
                id = data.Id.Value;
            }
            
            var device = new Device { Endpoint = data.Endpoint, Ip = data.Ip, Id = id};
            device.Endpoint.EndpointId = id;

            var capabilities = new List<Capability>();
            foreach (var capability in data.Capabilities)
            {
                if(capability.Equals("PowerController"))
                    capabilities.Add(new PowerController());
                if (capability.Equals("ColorController"))
                    capabilities.Add(new ColorController());
                if (capability.Equals("BrightnessController"))
                    capabilities.Add(new BrightnessController());
                if (capability.Equals("PercentageController "))
                    capabilities.Add(new PercentageController());
                if (capability.Equals("PowerLevelController "))
                    capabilities.Add(new PowerLevelController());
                if (capability.Equals("TemperatureSensor"))
                    capabilities.Add(new TemperatureSensor());
            }
            device.Endpoint.Capabilities = capabilities;

            if (data.Id.HasValue)
            {
                _deviceRepo.Update(device);
            }
            else
            {
                _deviceRepo.Add(device);
            }
            
            return RedirectToAction("List");
        }

        public IActionResult Edit(Guid id)
        {
            var device = _deviceRepo.FindById(id);

            var data = new AddDeviceModel
            {
                Id = device.Id,
                Ip = device.Ip,
                Endpoint = device.Endpoint,
                CapabilityData = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "PowerController", Value = "PowerController"},
                    new SelectListItem {Text = "ColorController", Value = "ColorController"},
                    new SelectListItem {Text = "BrightnessController", Value = "BrightnessController"},
                    new SelectListItem {Text = "PowerLevelController ", Value = "PowerLevelController "},
                    new SelectListItem {Text = "PercentageController ", Value = "PercentageController "},
                    new SelectListItem {Text = "TemperatureSensor", Value = "TemperatureSensor"}
                }, "Value", "Text")
            };

            foreach (var capability in data.CapabilityData)
            {
                foreach (var endpointCapability in device.Endpoint.Capabilities)
                {
                    if (capability.Text.Equals(endpointCapability.Interface.Substring(endpointCapability.Interface.LastIndexOf('.') + 1)))
                    {
                        capability.Selected = true;
                    }
                }
            }

            return View("Add", data);
        }

        public IActionResult Delete(Guid id)
        {
            _deviceRepo.DeleteById(id);
            return RedirectToAction("List");
        }
    }
}